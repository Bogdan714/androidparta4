package com.example.lenovo.androidparta4;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {
    @BindView(R.id.result)
    TextView result;

    @BindView(R.id.expression)
    EditText expression;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.button)
    public void onClick() {
        StringBuffer SB = new StringBuffer();
        String buffer = expression.getText().toString();
        buffer = buffer.replace(" ", "");
        String allowed = "0123456789+_*/";
        for (int i = 0; i < buffer.length(); i++) {
            if(!allowed.contains(buffer.substring(i,i+1))){
                return;
            }
            else{
                result.setText("result: "+recCalc(expression.getText().toString()));
            }
        }

    }

    public double recCalc(String expr) {
        if (expr.contains("+")) {
            return recCalc(expr.substring(0, expr.indexOf("+")))
                    + recCalc(expr.substring(expr.indexOf("+")+1, expr.length()));
        } else if (expr.contains("-")) {
            return recCalc(expr.substring(0, expr.indexOf("-")))
                    - recCalc(expr.substring(expr.indexOf("-")+1, expr.length()));
        } else if (expr.contains("/")) {
            return recCalc(expr.substring(0, expr.indexOf("/")))
                    / recCalc(expr.substring(expr.indexOf("/")+1, expr.length()));
        } else if (expr.contains("*")) {
            return recCalc(expr.substring(0, expr.indexOf("*")))
                    * recCalc(expr.substring(expr.indexOf("*")+1, expr.length()));
        } else return Integer.parseInt(expr);
    }
}
